import { Component, OnInit, AfterViewInit, Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { FormsModule } from '@angular/forms';


@Component({
  selector: 'car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css',
    '../shared-styles/widths-styles.css']
})

export class CarDetail implements OnChanges, AfterViewInit {
  @Input() carDetails: any;
  carDetailImage: any;
  carDetailDescription: any;
  carDetailName: any;
  carDetailLocation: any;
  carDetailPrice: any;


  constructor() {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.initializeData()


  }

  ngAfterViewInit() {

  }



  initializeData() {
    if (this.carDetails) {
      this.carDetailDescription = this.carDetails.description;
      this.carDetailImage = this.carDetails.main_image;
      this.carDetailName = this.carDetails.name;
    }
  }


}
