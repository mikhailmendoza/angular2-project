import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit
} from '@angular/core';
import 'rxjs/add/operator/map'
import * as _ from 'underscore';

import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CarService } from '../shared-service/car-service';
import { PagerService } from '../shared-service/pagination-service';

@Component({
  selector: 'car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css',
    '../shared-styles/widths-styles.css'],
  providers: [CarService, PagerService]
})


export class CarList implements OnInit, AfterViewInit {

  carListData: any[] = [];
  pager: any = {};
  pagedItems: any[];
  carDetails: any = {};
  mainPage = true;
  detailPage = false;

  constructor(private carService: CarService, private pagerService: PagerService) {

  }
  onChange(event): void {


  }

  ngOnInit(): void {

    this.carService.getAllCars().subscribe(
      (data: any) => {
        this.carListData = data;
      },
      (apiError: any) => {
        this.showApiErrorMsg(apiError);

      },
      () => {
        console.log(this.carListData);
        this.setPage(1);
      }
    );
  }
  ngAfterViewInit() {

  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.carListData.length, page);

    // get current page of items
    this.pagedItems = this.carListData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  viewDetails(evt: any) {
    this.carService.getCarDetailById(evt).subscribe(
      (data: any) => {
        this.carDetails = data;
      },
      (apiError: any) => {
        this.showApiErrorMsg(apiError);

      },
      () => {
        console.log(this.carDetails);
      }
    );
  }

  viewCarList(evt:any){
    if (evt !== ''){
      this.mainPage = false;
      this.detailPage = true;
      this.viewDetails(evt);
        
    }else{
      this.mainPage = true;
      this.detailPage = false;
      this.carDetails = {};
    }
  }

  showApiErrorMsg(msg: any) {
    console.log('AN ERROR OCCURRED! - ', msg);
  }
}