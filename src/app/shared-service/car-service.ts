import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class CarService {
  constructor(private http: Http) {}

  // GET Scenario Data
  getAllCars() {
    return this.http
      .get('http://localhost:3000/cars')
      .map((response: Response) => response.json())
      .do((data: any) => console.log('GET/carList', data))
      .catch(this.handleError);
  }

  getCarDetailById(carId: string): Observable<any> {
    const url = `http://localhost:3000/cars/${carId}`;
    return this.http
      .get(url)
      .map((response: Response) => response.json())
      .do((data: any) => console.log('GET/carDetail', data))
      .catch(this.handleError);
  }

  
  

  private handleError(error: Response) {
    console.log(error);
    const message = `Error status code ${error.status} at ${error.url}`;
    return Observable.throw(message);
  }
}
