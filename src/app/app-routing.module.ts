import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CarList }   from './car-list-component/car-list.component';


const routes: Routes = [
  { path: '', redirectTo: '/carList', pathMatch: 'full' },
  { path: 'carList', component: CarList },

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}